package VehicleNumber;
import java.util.Scanner;

public class VN {
    private static int count(int i) {
        int a, sum=0, temp=i;

        while(temp!=0){
            a=temp%10;
            sum+=a;
            temp=temp/10;
        }
        return sum;
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int lower = sc.nextInt();
        int higher = sc.nextInt();

        for (int i = lower; i < higher; i++) {

            count(i);
            if(count(i) == 9)
            {
                System.out.println(i);
            }

        }
    }

}
