package CalculateElectricityBill;
import java.util.Scanner;

public class CEB {
    public static void main(String[]args){

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the previous reading");
        int prev = sc.nextInt();
        System.out.println("Enter the current reading");
        int curr = sc.nextInt();
        int read = curr - prev;

        if(read <= 30){
            System.out.printf("Bill Amount is %.2f", (read*2.3) );
        }
        else if(read <= 100){
            System.out.printf("Bill Amount is %.2f", 69 + ((read-30)*3.5) );
        }
        else {
            System.out.printf("Bill Amount is %.2f", 314 + ((read-100)*4.60) );
        }


    }
}
