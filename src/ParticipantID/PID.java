package ParticipantID;
import java.util.Scanner;

public class PID {
    public static void main(String[] args) {
       Scanner sc = new Scanner(System.in);

       System.out.println("Enter the number participants");
        int n = sc.nextInt();

        System.out.println("Enter the IDs");
        int[] arr = new int[n];
        for(int i=0; i<n; i++)
        {
          arr[i] = sc.nextInt();
        }

        while(true){
            System.out.println("Enter the ID to search");
            int search = sc.nextInt();
            boolean found = false;

            if(search == 0)
                System.exit(0);

            for(int i=0; i<n; i++)
            {
                if(arr[i] == search){
                    found = true;
                    break;
                }
            }
            if(found)
            {
                System.out.println("Participant with ID "+search+" exists");
            }
            else
                System.out.println("Participant with ID "+search+" does not exist");

            }

        }


    }
