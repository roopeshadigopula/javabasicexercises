package BinarySearch;
import java.util.Scanner;

public class BS {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        //Array Length
        System.out.println("Enter array length");
        int n = sc.nextInt();

        //Creating Array
        System.out.println("Enter array elements");
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = sc.nextInt();
        }

        //Selection Sort
        System.out.println("Sorting Array.....,");
        selectionSort(arr);
        System.out.println("Array Sorted using Selection Sort");

        //Binary Search
        System.out.println("Enter a key to find using BinarySearch");
        int key = sc.nextInt();
        binarySearch(arr,0, n, key);


    }
    public static void selectionSort(int[] arr) {
        for (int i = 0; i < arr.length - 1; i++) {
            int index = i;
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[j] < arr[index]) {
                    index = j;
                }
            }
            int smallerNumber = arr[index];
            arr[index] = arr[i];
            arr[i] = smallerNumber;
        }
    }
    public static void binarySearch(int arr[], int first, int last, int key){
        int mid = (first + last)/2;
        while( first <= last ){
            if ( arr[mid] < key ){
                first = mid + 1;
            }else if ( arr[mid] == key ){
                System.out.println("Element is found at index after sorting: " + mid);
                break;
            }else{
                last = mid - 1;
            }
            mid = (first + last)/2;
        }
        if ( first > last ){
            System.out.println("Element is not found!");
        }
    }
}