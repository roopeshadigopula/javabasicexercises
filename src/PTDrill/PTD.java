package PTDrill;
import java.util.Arrays;
import java.util.Scanner;
import java.lang.Math;

public class PTD {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter Number of Students");
        int num = sc.nextInt();
        int n = (int)Math.sqrt(num);
        int count=0;
        String[] arr = new String[num];
        String dim[][] = new String[n][n];

        if (num % 2 == 0 || num < 9) {
            System.out.println("Number of students should be square of an odd number");
            System.exit(0);
        }
        else {
            //Names stored in array
            System.out.println("Enter Names");
            for (int a = 0; a < num; a++) {
                arr[a] = sc.next();
            }

            //Sorting names array
            Arrays.sort(arr);

            //Storing names in 2d array
            for(int i=0; i<n; i++){
                for(int j=0; j<n; j++,count++){
                    dim[i][j] = arr[count];
                }
            }

//            for(int i=0; i<n; i++){
//                for(int j=0; j<n; j++){
//                    System.out.print(dim[i][j]+" ");
//                }
//                System.out.println();
//            }
//            System.out.println();

            //Printing Middle row and column
            System.out.print("Middle row:\t");
            for(int i=n/2, j=0; j<n; j++)
            {
                System.out.print(dim[i][j] + " ");
            }
            System.out.println();
            System.out.print("Middle column:\t");
            for(int i=0, j=n/2; i<n; i++) {
                System.out.print(dim[i][j] + " ");
            }
        }
    }
}
