package SpecialSequence;
import java.util.Scanner;

public class SS {
    public static void main(String[] args) {
        int a=2, b=1, c=3, sum=0;

        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();

        if(n>0)
        {
            switch(n)
            {
                case 1:
                    System.out.print(b);
                    break;
                case 2:
                    System.out.print(a);
                    break;
                case 3:
                case 4:
                case 5:
                    System.out.print(a+" "+b);
                    break;
                case 6:
                    System.out.print(a+" "+b+" "+c+" ");
                    break;
                default:
                    System.out.print(a+" "+b+" "+c+" ");
                    while(sum<n)
                    {
                        sum = a+b+c;
                        if(sum<n)
                            System.out.print(sum+" ");
                        a=b;
                        b=c;
                        c=sum;
                    }
                    break;
            }
        }

    }
}
