package CircleData;
import java.util.Scanner;

public class CD{

    public static void main(String[] args) {
        final double pi = 3.14;
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the Radius");
        float rad = sc.nextFloat();

        System.out.println("Enter the Output required");
        sc.nextLine();
        String op = sc.nextLine();

        switch(op)
        {
            case "DIA":
                System.out.printf("Diameter of circle is %.2f", 2*rad);
                break;
            case "AR":
                System.out.printf("Area of circle is %.2f", pi*rad*rad);
                break;
            case "PER":
                System.out.printf("Perimeter of circle is %.2f", 2*pi*rad);
                break;
            case "ARSEM":
                System.out.printf("Area of Semi Circle is %.2f", pi*rad);
                break;
        }
    }
}
